import { Component, OnInit } from '@angular/core';

import { Recipe } from '../recipe.model'

@Component({
  selector: 'app-rlist',
  templateUrl: './rlist.component.html',
  styleUrls: ['./rlist.component.css']
})
export class RlistComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('Spaghetti', 'Spaghetti Bolognese', 'https://static.pexels.com/photos/8500/food-dinner-pasta-spaghetti-8500.jpg'),
    new Recipe('Chaczapuri', 'Chaczapuri imeruli', 'https://upload.wikimedia.org/wikipedia/commons/8/88/Hachapori.jpg')
  ];
  constructor() {
    console.log(this.recipes[0].name)
    console.log(this.recipes[0].description)
    console.log(this.recipes[0].imagePath)
  }

  ngOnInit() {
  }

}
