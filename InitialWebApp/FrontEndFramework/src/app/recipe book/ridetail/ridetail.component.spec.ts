import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RidetailComponent } from './ridetail.component';

describe('RidetailComponent', () => {
  let component: RidetailComponent;
  let fixture: ComponentFixture<RidetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RidetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RidetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
