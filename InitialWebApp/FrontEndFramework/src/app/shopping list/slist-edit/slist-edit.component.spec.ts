import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlistEditComponent } from './slist-edit.component';

describe('SlistEditComponent', () => {
  let component: SlistEditComponent;
  let fixture: ComponentFixture<SlistEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlistEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlistEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
